const express = require('express')
const dotenv = require('dotenv')
const router = require('./src/routes/main')

const cors = require('cors')

var corsOptions = {
    origin: '*', 
    optionsSuccessStatus: 200 
  }
  
//get config vars
dotenv.config()
const app = express()
const port = 8080


app.use(cors(corsOptions))
app.use(express.json())
app.use("/", router)

app.listen(port, () => {
    console.log(`app listening on port ${port}`)
})