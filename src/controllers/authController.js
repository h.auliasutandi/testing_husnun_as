const { PrismaClient } = require('@prisma/client')
const bcrypt = require("bcryptjs")
const jwt = require('jsonwebtoken');
const { getUser } = require('../utils/helper')

const prisma = new PrismaClient()


const register = async (req,res)=>{
	console.log('masuk ke register')
  let {username, email, password} = req.body

  const hashedPassword = await bcrypt.hash(password, 10)

  const user = await prisma.user.create({data: {username, email, password: hashedPassword}})

  const expiresIn = 2*24*3600 //2 days 

  const token = jwt.sign({userId: user.id}, process.env.TOKEN_SECRET, { expiresIn});

  return res.json({user: {id: user.id, username, email}, token})
}

const login = async (req,res)=>{
  let {username, email, password} = req.body

  const users = await prisma.user.findMany({
    where: {
      OR: [
        {username},
        {email}
      ]
    }
  })

  if (users.length > 0 ){
    let user = users[0]
    const valid = bcrypt.compare(password, user.password)
    if(!valid){
      return res.json("invalid credentials")
    }else{
      const expiresIn = 2*24*3600 //2 days 

      const token = jwt.sign({userId: user.id}, process.env.TOKEN_SECRET, { expiresIn});
      const {id, username, email} = user

      return res.json({user: {id, username, email}, token})
    }
  }else{
    return res.json("invalid credentials")
  }
}


module.exports ={
	register, login
  }
  