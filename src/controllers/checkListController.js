const { PrismaClient } = require('@prisma/client')

const prisma = new PrismaClient()

const getAllChecklistItemByCheckListId = async (req, res) => {
    let {checklistId} = req.params
    checklistId = Number(checklistId)
    try{
        const existingChecklistId = await prisma.checklist.findFirst({ where: { checklistId } });
        
        if (!existingChecklistId) {
            return res.status(404).json({ error: "Checklist item not found" });
        }

        const dataCheckList = await prisma.checklist.findFirstOrThrow({where: {checklistId}})
        return res.status(200).json(dataCheckList)
    }catch(err){
        return res.status(404).json({error:err})
    }
}

const createNewChecklistItemInChecklist = async (req, res) => {
    let {itemName} = req.body
    let {checklistId} = req.params
    checklistId = Number(checklistId)
    
    try {
        const existingChecklistId = await prisma.checklist.findFirst({ where: { checklistId } });
        
        if (!existingChecklistId) {
            return res.status(404).json({ error: "Checklist item not found" });
        }

        const dataChecklist = await prisma.checklist.create({
            data: {itemName,checklistId}
        })
        return res.status(200).json(dataChecklist)
    } catch (error) {
        return res.status(400).json({error: error})
    }
}

const getChecklistItemInChecklistByChecklistId = async (req, res) => {
    let {checklistId,checklistItemId} = req.params
    checklistId = Number(checklistId)
    checklistItemId = Number(checklistItemId)
    
    try{
        const existingChecklistItem = await prisma.checklist.findFirst({ where: { checklistId, checklistItemId } });
        
        if (!existingChecklistItem) {
            return res.status(404).json({ error: "Checklist item not found" });
        }

        const dataCheckList = await prisma.checklist.findFirstOrThrow({where: {checklistId,checklistItemId}})
        return res.status(200).json(dataCheckList)
    }catch(err){
        return res.status(404).json({error:err})
    }
}

const UpdateStatusChecklistItemByChecklistItemId = async (req, res) => {
    let { checklistId, checklistItemId } = req.params;
    checklistId = Number(checklistId);
    checklistItemId = Number(checklistItemId);
    
    try {
        const existingChecklistItem = await prisma.checklist.findFirst({ where: { checklistId, checklistItemId } });
        
        if (!existingChecklistItem) {
            return res.status(404).json({ error: "Checklist item not found" });
        }

        const updatedChecklistItem = await prisma.checklist.update({
            where: { checklistId, checklistItemId },
            data: { status: true }
        });

        return res.status(200).json(updatedChecklistItem);
    } catch (err) {
        return res.status(500).json({ error: err.message });
    }
};


const DeleteItemByChecklistItemId = async (req, res) => {
    let { checklistId, checklistItemId } = req.params;
    checklistId = Number(checklistId);
    checklistItemId = Number(checklistItemId);
    
    try {
        const existingChecklistItem = await prisma.checklist.findFirst({ where: { checklistId, checklistItemId } });
        
        if (!existingChecklistItem) {
            return res.status(404).json({ error: "Checklist item not found" });
        }

        const DeleteItemByChecklistItemId = await prisma.checklist.delete({
            where: { checklistId, checklistItemId },
        });

        return res.status(200).json(DeleteItemByChecklistItemId);
    } catch (err) {
        return res.status(500).json({ error: err.message });
    }
};

const RenameItemByCheclistItemId = async (req, res) => {
    try {
        let {itemName} = req.body
        let { checklistId, checklistItemId } = req.params;
        checklistId = Number(checklistId);
        checklistItemId = Number(checklistItemId);
        const existingChecklistItem = await prisma.checklist.findFirst({ where: { checklistId, checklistItemId } });
        
        if (!existingChecklistItem) {
            return res.status(404).json({ error: "Checklist item not found" });
        }

        const RenameItemByCheclistItemId = await prisma.checklist.update({
            where: { checklistId, checklistItemId },
            data: { itemName }
        });

        return res.status(200).json(RenameItemByCheclistItemId);
    } catch (err) {
        return res.status(500).json({ error: err.message });
    }
}

module.exports = { 
    getAllChecklistItemByCheckListId,
    createNewChecklistItemInChecklist,
    getChecklistItemInChecklistByChecklistId,
    UpdateStatusChecklistItemByChecklistItemId,
    DeleteItemByChecklistItemId,
    RenameItemByCheclistItemId,
};
