const express = require("express");
const router = express.Router();

const verifyToken = require("../middleware/auth");
const { register, login } = require("../controllers/authController");
const { getAllChecklistItemByCheckListId, createNewChecklistItemInChecklist, UpdateStatusChecklistItemByChecklistItemId, DeleteItemByChecklistItemId } = require("../controllers/checkListController");

// main
router.get("/", (req, res) => {
    res.send("Hello , Ecommerce API for Admin is Running !");
});

router.post("/login", login);
router.post("/register", register);


router.get('/checklist/:checklistId/item',verifyToken, getAllChecklistItemByCheckListId )
router.post('/checklist/:checklistId/item',verifyToken,createNewChecklistItemInChecklist)
router.get('/checklist/:checklistId/item/:checklistItemId',verifyToken, getAllChecklistItemByCheckListId )
router.put('/checklist/:checklistId/item/:checklistItemId',verifyToken, UpdateStatusChecklistItemByChecklistItemId)
router.delete('/checklist/:checklistId/item/:checklistItemId',verifyToken ,DeleteItemByChecklistItemId)
router.put('/checklist/:checklistId/item/rename/:checklistItemId',verifyToken, UpdateStatusChecklistItemByChecklistItemId)

module.exports = router