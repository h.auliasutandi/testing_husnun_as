const jwt = require("jsonwebtoken");
const verifyToken = (req, res, next) => {
	const bearerHeader = req.headers["authorization"];
	if (typeof bearerHeader !== "undefined") {
		const bearer = bearerHeader.split(" ");
		const token = bearer[1];
		jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
			if (err) return res.status(400).json({ error: "invalid token" });

			req.user = user;

			next();
		});
	} else {
		res.status(403).json({ msg: "forbidden" });
	}
};

module.exports = verifyToken;
